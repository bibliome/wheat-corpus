from tqdm.auto import tqdm
import re, os, argparse
from torch.utils.data import random_split

def brat_to_iob(path_from, path_to, rename_entities_types=dict(), token_column=0, ner_column=1, columns_sep=" ", phrase_sep="\n", nel_annotation_start="#", annotations_extention=".ann", text_extention=".txt", split_data={
    'train':0.8, 'validation':0.1, 'test':0.1}):

    def next_(iterator):
        try: return next(iterator)
        except StopIteration: pass

    for root, dirs, files in os.walk(path_from):
        if not(dirs): dirs = ["dataset"]
        result = {dir:{"input":[], "labels":[]} for dir in dirs}
        for file in files:
            if file.endswith(text_extention):
                with open(root+"/"+file) as doc: lines = doc.readlines()
                text = "".join(lines)
                spaces = [re.finditer("\s+", line) for line in lines]
                inputs = [re.split(r'([!"$%\')+,-./:;<=>?\]}~]*)[\s]+(["\'(\[{]*)', i) for i in lines]
                inputs = [[token for token in line if token] for line in inputs]
                gold = [["O"] * len(line) for line in inputs]
                end_of_token_indeces = []
                l = 0
                for line, space_indeces in zip(inputs, spaces):
                    nearest_space = next(space_indeces)
                    shift = l
                    if nearest_space.start() == 0:
                        l += nearest_space.end()
                        nearest_space = next_(space_indeces) or nearest_space
                    end_of_token_indeces.append([])
                    for token in line:
                        l+=len(token)
                        end_of_token_indeces[-1].append(l)
                        while nearest_space.start() + shift == l:
                            l += nearest_space.end() - nearest_space.start()
                            nearest_space = next_(space_indeces) or nearest_space
                for i, end in enumerate(reversed(end_of_token_indeces)):
                    if end:
                        last_non_empty = -(i+1)
                        break
                assert end_of_token_indeces[last_non_empty][-1] == len(text.rstrip())
                with open(root+"/"+file.replace(text_extention, annotations_extention)) as annotations:
                    for annotation in annotations:
                        if not(annotation.startswith(nel_annotation_start)):
                            _, info, token = annotation.strip().split("\t")
                            entity = info[:info.index(" ")]
                            rename_entities_types[entity] = rename_entities_types.get(entity, entity.upper())
                            relevant_indeces = info[info.index(" ")+1:] #we have a NE before the first whitespace
                            for token_piece in relevant_indeces.split(";"):
                                start, end = token_piece.split()
                                found, finished = False, False
                                for i, line in enumerate(end_of_token_indeces):
                                    if not finished:
                                        for j, index in enumerate(line):
                                            if not(found):
                                                if index > int(start):
                                                    gold[i][j] = "B-"+rename_entities_types[entity]
                                                    found = True
                                            elif not(finished):
                                                if index <= int(end):
                                                    gold[i][j] = "I-"+rename_entities_types[entity]
                                                else:
                                                    finished = True
                                                    break
                for dir in dirs:
                    if dir in root or dirs == ["dataset"]:
                        result[dir]["input"] += [inputs]
                        result[dir]["labels"] += [gold]
        if not(os.path.isdir(path)): os.mkdir(path_to)
        if split_data:
            dataset = [(i, l) for sample in result.values() for i, l in zip(*sample.values())]
            dataset_size = len(dataset)
            sizes = {name:int(dataset_size*proportion) for name, proportion in split_data.items()}
            sizes["test"] = sizes.get("test", 0) + dataset_size - sum(sizes.values())
            splits = random_split(dataset, [v for k, v in sorted(sizes.items())])
            result = {name:dict(zip(["input", "labels"], zip(*list(split)))) for name, split in zip(sorted(sizes), splits)}
        for file, annotations in result.items():
            with open(path_to+"/"+file+".txt", "w") as outp:
                for text_of_tokens, text_of_entities in zip(annotations["input"], annotations["labels"]):
                    for tokens, entities in zip(text_of_tokens, text_of_entities):
                        for token, entity in zip(tokens, entities):
                            ordered = ["", ""]
                            token = token.replace("\n", "")
                            if token:
                                ordered[token_column] = token
                                ordered[ner_column] = entity
                                outp.write(columns_sep.join(ordered)+"\n")
                        outp.write(phrase_sep)



parser = argparse.ArgumentParser()
parser.add_argument('--input', type=str, help="Path to a corpus in BRAT format.")
parser.add_argument('--output', type=str, help="Path to save the IOB annotations.")
parser.add_argument('--split', nargs="?", type=str, help="Proportion of train, validation and test samples separated by ','. For example, 80,10,10 means you'll have 80% of train, 10% of val and 10% of test samples.")
args = parser.parse_args()
if args.split:
    percentages = args.split.split(",")
    decimals = [int(perc)/100 for perc in percentages]
    assert(sum(decimals) == 1)
    splits = ["train", "validation", "test"]
    split_data = dict(zip(splits, decimals))
else:
    split_data = {"train":0.8, "validation":0.1, "test":0.1}
brat_to_iob(args.input, args.ourput, split_data=split_data)


