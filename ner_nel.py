from tqdm.auto import tqdm
import re, os, argparse, json
from torch.utils.data import random_split




parser = argparse.ArgumentParser()
parser.add_argument('--input', type=str, help="Path to saved predictions.")
parser.add_argument('--output', type=str, help="Path to save NER  annotations.")
args = parser.parse_args()
#%%
to_nel = []
ann_id = 0
with open(args.input) as preds:
    for pred in preds:
        p = eval(pred)
        for i, l in zip(p["input"], p["prediction"]):
            if l in {"B-TRAIT", "B-PHENOTYPE"}:
                ann_id += 1
                to_nel.append([i])
            elif l in {"I-TRAIT", "I-PHENOTYPE"}:
                to_nel[-1].append(i)
with open(args.output, "w") as pred:
    json.dump({i:mention for i, mention in enumerate(to_nel, 1)}, pred)


