import json, pronto, argparse
from sklearn.metrics import f1_score, accuracy_score, precision_recall_fscore_support
from pandas import DataFrame as df


def preprocess_C_norm_outp(gold_path="wto_norm_test.json", pred_path="outputs/wto.txt"):
    with open(gold_path) as ann: gold = {i: label[0] for i, label in json.load(ann).items()}
    with open(pred_path) as predictions: pred = {i:label for i, label, cos in map(lambda line: line.split(), predictions)}
    del pred["mentionId"]
    return gold, pred


def transform(data, ontology):
    terms = {term.id:i for i, term in enumerate(ontology.terms())}
    transformed = [terms[label] for i, label in sorted(data.items())]
    return transformed


def get_root(ontology):
    for term in ontology.terms():
        if len(set(term.superclasses())) == 1 and not(term.obsolete):
            return term


def get_lca(term1, term2):
    ancestors1, ancestors2 = set(term1.superclasses()), set(term2.superclasses())
    common_ancestors = ancestors1 & ancestors2
    lca = list(filter(lambda term: set(term.subclasses()) & common_ancestors == {term}, common_ancestors))
    try:
        assert len(lca) == 1
        return lca[0]
    except AssertionError:
        if len(lca) == 0: return None
        else: return lca[0]#raise AssertionError


def get_distance(term, relative, mode="anc"):
    search = {"anc":term.superclasses, "desc":term.subclasses}[mode]
    distance = 0
    while relative not in search(distance=distance): distance += 1
    return distance


def S(term, relative, root, mode="anc"):
    w = {"anc": 0.6, "desc": 0.8}[mode]
    distance = get_distance(term, relative, mode=mode)
    return 0 if relative == root and distance == 0 else w ** distance


def SV(term, root, mode="anc"):
    w = {"anc": 0.6, "desc": 0.8}[mode]
    if mode == "anc":
        distance = get_distance(term, root, mode=mode)
        return (1 - w ** (distance + 1)) / (1 - w)
    elif mode == "desc":
        result = 0
        for child in term.subclasses():
            result += S(term, child, root, mode=mode)
        return result


def wang_similarity(terms, onto):
    for term1id, term2id in terms:
        term1, term2 = onto[term1id], onto[term2id]
        ancestors1, ancestors2 = set(term1.superclasses()), set(term2.superclasses())
        common_ancestors = ancestors1 & ancestors2
        numerator = 0
        if common_ancestors:
            for ancestor in common_ancestors:
                numerator += S(term1, ancestor, onto.root) + S(term2, ancestor, onto.root)
            denominator = SV(term1, onto.root) + SV(term2, onto.root)
            yield numerator / denominator
        else: yield 0


def wu_palmer(terms, onto):
    for term1id, term2id in terms:
        term1, term2 = onto[term1id], onto[term2id]
        lca = get_lca(term1, term2)
        if lca:
            depth_lca, depth_term1, depth_term2 = get_distance(lca, onto.root), get_distance(term1, onto.root), get_distance(term2, onto.root)
            yield 2*depth_lca/(depth_term1+depth_term2)
        else: yield 0


def sim_desc(term1, term2, root):
    desc1, desc2 = term1.subclasses(), term2.subclasses()
    common_desc = set(desc1) & set(desc2)
    if common_desc:
        numerator = 0
        for desc in common_desc:
            numerator += S(term1, desc, root, mode="desc") + S(term2, desc, root, mode="desc")
        denominator = SV(term1, root, mode="desc") + SV(term2, root, mode="desc")
        return numerator / denominator
    else:
        return 0


def sim_lca(term1, term2, root):
    lca = get_lca(term1, term2)
    if lca:
        sv_lca, sv1, sv2 = SV(lca, root), SV(term1, root), SV(term2, root)
        return 2*sv_lca/(sv1+sv2)
    else: return 0


def simsim(terms, onto):
    for term1, term2 in terms:
        term1, term2 = onto[term1], onto[term2]
        yield sim_lca(term1, term2, onto.root) * 0.5 + sim_desc(term1, term2, onto.root) * 0.5


def measure(gold, pred, onto):
    data = list(zip([label for i, label in sorted(gold.items())], [label for i, label in sorted(pred.items())]))
    gold, pred = transform(gold, onto), transform(pred, onto)
    precision, recall, fbeta, support = precision_recall_fscore_support(gold, pred, average="weighted")
    f_micro = f1_score(gold, pred, average="micro")
    f_macro = f1_score(gold, pred, average="macro")
    root = get_root(onto)
    setattr(onto, "root", root)
    wang = list(wang_similarity(terms=data, onto=onto))
    wang = sum(wang)/len(wang)
    wp = list(wu_palmer(terms=data, onto=onto))
    wp = sum(wp)/len(wp)
    sim = list(simsim(terms=data, onto=onto))
    sim = sum(sim)/len(sim)
    return {"f_micro": f_micro, "f_macro": f_macro, "f_weighted": fbeta, "accuracy": accuracy_score(gold, pred), "precision":precision, "recall":recall, 'wang_similarity': wang,
            'wu_palmer_distance': wp,
            'sem_sim':sim}


def save(result, path): df({"":result}).to_csv(path)


if __name__=="__main__":
    parser = argparse.ArgumentParser(description='Evaluate NEL annotations.')
    parser.add_argument('--pred', type=str, help='Path to a file with predicted annotations. Every prediction should be on a separate line and follow the format: "<annotation id>\t<ontology concept>"')
    parser.add_argument('--gold', type=str, help='Path to a file with correct annotations. Every prediction should be on a separate line and follow the format: "<annotation id>\t<ontology concept>"')
    parser.add_argument('--onto', type=str, help='Path to ontology.')
    parser.add_argument('--path_to_save', type=str, nargs="?", default="NEL_results.csv", help='Path to save the results.')
    args = parser.parse_args()
    with open(args.gold) as file: gold = dict(line.strip().split() for line in file.readlines())
    with open(args.pred) as file: pred = dict(line.strip().split() for line in file.readlines())
    onto = pronto.Ontology(args.onto)
    result = measure(gold, pred, onto)
    save(result, args.path_to_save)