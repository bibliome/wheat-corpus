import argparse, os, shutil
from torch.utils.data import random_split



parts = ["train", "test", "dev", "train_dev"]
for part in parts:
    try: shutil.rmtree(part)
    except FileNotFoundError: pass
    os.mkdir(part)
files = list(filter(lambda f:f.endswith(".concept"), os.listdir("./all")))
splits = random_split(files, [0.8, 0.1, 0.1])
for part, split in zip(parts[:-1], splits):
    for f in split:
        shutil.copyfile("./all/"+f, part+"/"+f)
for folder in ["train", "dev"]:
    for f in os.listdir(folder): shutil.copyfile(folder+"/"+f, "train_dev/"+f)
for part in parts[:-1]:
    concepts = set()
    with open(part+"_dictionary.txt", "w") as dictionary:
        for file in os.listdir(part):
            with open(part+"/"+file) as anns:
                for ann in anns:
                    *_, word, i = ann.strip().split("||")
                    concepts.add(i+"||"+word)
        for concept in sorted(concepts): dictionary.write(concept+"\n")