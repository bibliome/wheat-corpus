import re, os, argparse, json
from torch.utils.data import random_split

for ontology in ["wto", "ncbi"]:
    for mode in ["mentions", "norm"]:
        file = f"{ontology}_{mode}"
        with open(file+".json") as ann: anns = json.load(ann)
        splits = random_split(list(anns.keys()), [0.8, 0.2])
        for part, split in zip(["train", "test"], splits):
            final = {k:v for k, v in anns.items() if k in split}
            with open(file + "_"+part+".json", "w") as outp: json.dump(final, outp)
           