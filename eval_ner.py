import argparse
from tner import TransformersNER
from pandas import DataFrame


parser = argparse.ArgumentParser()
parser.add_argument('--model', type=str, help="Path to a model to evaluate.")
parser.add_argument('--data', type=str, help="Path to a dataset. It should be a folder that contains a file 'test.txt' with the IOB annotations.")
parser.add_argument('--save', nargs="?", type=str, help="Path to save the results")
args = parser.parse_args()
model = TransformersNER(args.model)
dataset_root = args.data
data = {"test":args.data+"/test.txt"}
metric = model.evaluate(local_dataset=data, dataset_split='test', batch_size=32, cache_file_prediction="./predicted_bio.txt")
if args.save:
    overall = DataFrame({k:[v] for k, v in metric.items() if v}, index=[model.model.config.model_type]).drop(columns=["per_entity_metric"])
    per_entity = DataFrame(metric["per_entity_metric"]).drop("f1_ci")
    overall.transpose().to_csv(f"{args.save}/overall.csv")
    per_entity.to_csv(f"{args.save}/per_entity.csv")
print(metric)
