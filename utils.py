from pandas import concat, read_csv
import os, re, json, pronto
import numpy as np
from gensim.models import KeyedVectors, Word2Vec
from sklearn.metrics import accuracy_score, precision_recall_fscore_support
from torch.utils.data import random_split

def tokenize(path="../wheat-dataset_FULL_2023-05-25/"):
    corpus = []
    for file in os.listdir(path):
        if file.endswith(".txt"):
            with open(path+file) as text:
                lines = [line.strip().lower()+" " for line in text.readlines()]
                tokens = [re.split(r'([!"$%\')+,-./:;<=>?\]}~]*)[\s]+(["\'(\[{]*)', i) for i in lines]
                tokens = [[token for token in line if token] for line in tokens]
                corpus.append(tokens[0])
    return corpus

def fine_tune(model_path="BioWordVec_PubMed_MIMICIII_d200.vec.bin", corpus_path="../wheat-dataset_FULL_2023-05-25/", save_path="wheat.model"):
    corpus = tokenize(corpus_path)
    base = KeyedVectors.load_word2vec_format(model_path, binary=True)
    new = Word2Vec(vector_size=base.vector_size, min_count=1)
    new.build_vocab(corpus)
    new.build_vocab([list(base.key_to_index.keys())], update=True)
    new.wv.vectors_lockf = np.ones(len(new.wv))
    new.wv.intersect_word2vec_format("BioWordVec_PubMed_MIMICIII_d200.vec.bin", binary=True)
    new.train(corpus, total_examples=len(corpus), epochs=10)
    new.save(save_path)
    return new

def assemble(output_path, *paths):
    concat([read_csv(path, index_col=0) for path in paths], axis=1).to_csv(output_path)