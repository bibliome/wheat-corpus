import argparse
from tner import TransformersNER, GridSearcher

parser = argparse.ArgumentParser()
parser.add_argument('--model', type=str, help="Name of a model to train.")
parser.add_argument('--data', type=str, help="Path to a dataset. It should be a folder that contains 'train.txt', 'validation.txt' and 'test.txt' with the IOB annotations.")
parser.add_argument('--save', nargs="?", type=str, help="Path to save the model")
args = parser.parse_args()

dataset = {split:f"{args.data}/{split}.txt" for split in ["train", "validation", "test"]}

searcher = GridSearcher(
    checkpoint_dir=args.save,
    local_dataset=dataset,
    model=args.model,
    epoch=10,
    epoch_partial=5,
    n_max_config=1,
    batch_size=64,
    gradient_accumulation_steps=[2],
    crf=[True],
    lr=[2e-5, 3e-5, 5e-5],
    weight_decay=[None],
    random_seed=[42],
    lr_warmup_step_ratio=[0.1],
    max_grad_norm=[None, 10]
)
searcher.train()
